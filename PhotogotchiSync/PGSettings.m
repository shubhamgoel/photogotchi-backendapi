//
//  PGSettings.m
//  PhotogotchiSync
//
//  Created by Borbás Geri on 9/1/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import "PGSettings.h"


@implementation PGSettings

@synthesize saveToLibraryNumber = _saveToLibraryNumber;
@synthesize facebookIsOnNumber = _facebookIsOnNumber;
@synthesize twitterIsOnNumber = _twitterIsOnNumber;
@synthesize tumblrIsOnNumber = _tumblrIsOnNumber;

@synthesize saveToLibrary = _saveToLibrary;
@synthesize facebookIsOn = _facebookIsOn;
@synthesize twitterIsOn = _twitterIsOn;
@synthesize tumblrIsOn = _tumblrIsOn;


#pragma mark - NSNumber wrappers for clients

-(void)setSaveToLibrary:(BOOL)saveToLibrary { self.saveToLibraryNumber = [NSNumber numberWithBool:saveToLibrary];}
-(void)setFacebookIsOn:(BOOL)facebookIsOn { self.facebookIsOnNumber = [NSNumber numberWithBool:facebookIsOn];}
-(void)setTwitterIsOn:(BOOL)twitterIsOn { self.twitterIsOnNumber = [NSNumber numberWithBool:twitterIsOn];}
-(void)setTumblrIsOn:(BOOL)tumblrIsOn { self.tumblrIsOnNumber = [NSNumber numberWithBool:tumblrIsOn];}

//TODO:Debug these bastards along frontend logic.
-(BOOL)saveToLibrary { return [self.saveToLibraryNumber boolValue]; }
-(BOOL)facebookIsOn { return YES; /*[self.facebookIsOnNumber boolValue];*/ }
-(BOOL)twitterIsOn { return YES; /*[self.twitterIsOnNumber boolValue];*/ }
-(BOOL)tumblrIsOn { return YES; /*[self.tumblrIsOnNumber boolValue];*/ }

@end
