//
//  PGUserCounters.m
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import "PGUserCounters.h"

@implementation PGUserCounters

@synthesize photo = _photo;
@synthesize daytimePhoto = _daytimePhoto;
@synthesize morningPhoto = _morningPhoto;
@synthesize nightPhoto = _nightPhoto;
@synthesize rearPhoto = _rearPhoto;
@synthesize frontPhoto = _frontPhoto;
@synthesize launch = _launch;
@synthesize rename = _rename;
@synthesize refer = _refer;

@synthesize facebookShare = _facebookShare;
@synthesize twitterShare = _twitterShare;
@synthesize tumblrShare = _tumblrShare;


@end
