//
//  PGCreatureData.m
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import "PGCreatureData.h"

@implementation PGCreatureData

@synthesize ID = _ID;
@synthesize teamID = _teamID;
@synthesize name = _name;
@synthesize points = _points;
@synthesize level = _level;
@synthesize birthdayDate = _birthdayDate;

@end
