//
//  PGTeamData.m
//  PhotogotchiSync
//
//  Created by Borbás Geri on 9/1/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import "PGTeamData.h"

@implementation PGTeamData

@synthesize ID = _ID;
@synthesize userCount = _userCount;
@synthesize photoCount = _photoCount;
@synthesize facebookShareCount = _facebookShareCount;
@synthesize twitterShareCount = _twitterShareCount;
@synthesize tumblrShareCount = _tumblrShareCount;

@end
