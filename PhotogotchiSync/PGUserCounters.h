//
//  PGUserCounters.h
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGUserCounters : NSObject

@property (nonatomic) int photo;
@property (nonatomic) int daytimePhoto;
@property (nonatomic) int morningPhoto;
@property (nonatomic) int nightPhoto;
@property (nonatomic) int rearPhoto;
@property (nonatomic) int frontPhoto;
@property (nonatomic) int launch;
@property (nonatomic) int rename;
@property (nonatomic) int refer;

@property (nonatomic) int facebookShare;
@property (nonatomic) int twitterShare;
@property (nonatomic) int tumblrShare;

@end
