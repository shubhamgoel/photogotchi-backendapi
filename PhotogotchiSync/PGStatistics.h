//
//  PGStatistics.h
//  PhotogotchiSync
//
//  Created by Borbás Geri on 9/1/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface PGStatistics : NSObject

@property (nonatomic, strong) NSMutableDictionary *teams;

@end
