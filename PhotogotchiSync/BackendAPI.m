//
//  BackendAPI.m
//  PhotogotchiSync
//
//  Created by Shubham Goel on 31/08/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import "BackendAPI.h"
#import "AFJSONRequestOperation.h"
#import "NSString+md5.h"


@interface BackendAPI ()
@property (nonatomic, strong) NSDictionary *keysAdapter;
@property (nonatomic, strong) NSDictionary *mandatoryFields;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;

-(NSString*)backendKeyForClientKey:(NSString*) key;

@end


@implementation BackendAPI

@synthesize keysAdapter = _keysAdapter;
@synthesize mandatoryFields = _mandatoryFields;
@synthesize dateFormatter = _dateFormatter;

@synthesize userDefaults = _userDefaults;
@synthesize teamStatistics = _teamStatistics;
@synthesize delegate = _delegate;


#pragma mark - Creation

+(id)backendWithDelegate:(id) delegate
{
    return [[self alloc] initWithDelegate:delegate];
}

-(id)initWithDelegate:(id) delegate
{
    if (self = [super init])
    {
        self.dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"YYYY-MM-DD hh:mm:ss"];
        
        self.delegate = delegate;
    }
    return self;
}



#pragma mark - Keys adapter (will remove once backend code gets updated to current structure)

    #define COMING_SOON [NSNull null]

-(NSDictionary*)keysAdapter
{
    if (_keysAdapter != nil) return _keysAdapter;
    
    NSDictionary *keysAdapter = [NSDictionary dictionaryWithObjectsAndKeys:
                              
                              COMING_SOON,                  @"user.UDID",
                              @"first_photo_location",      @"user.firstPhotoLocationString",
                              @"first_photo_date",          @"user.firstPhotoDate",
                              
                              @"creature_id",               @"user.creature.ID",
                              @"creature_id",               @"user.creature.team_id",                                 
                              @"name",                      @"user.creature.name",
                              @"rating",                    @"user.creature.points",
                              COMING_SOON,                  @"user.creature.level",
                              COMING_SOON,                  @"user.creature.birthdayDate",
                              
                              @"photo_count",               @"user.totalCount.photo",
                              COMING_SOON,                  @"user.totalCount.daytimePhoto",
                              COMING_SOON,                  @"user.totalCount.morningPhoto",
                              COMING_SOON,                  @"user.totalCount.nightPhoto",
                              COMING_SOON,                  @"user.totalCount.rearPhoto",
                              COMING_SOON,                  @"user.totalCount.frontPhoto",
                              COMING_SOON,                  @"user.totalCount.launch",
                              COMING_SOON,                  @"user.totalCount.rename",
                              COMING_SOON,                  @"user.totalCount.refer",
                              @"facebook_share_count",      @"user.totalCount.facebookShare",
                              @"twitter_share_count",       @"user.totalCount.twitterShare",                         
                              @"tumblr_share_count",        @"user.totalCount.tumblrShare",                       
                              
                              nil];
    
    _keysAdapter = keysAdapter;
    return _keysAdapter;
}

-(NSString*)backendKeyForClientKey:(NSString*) key
{
    return [self.keysAdapter objectForKey:key];
}


#pragma mark - Team statistics

-(void)getStatistics
{
    /*
    
    //download team statistics.
    NSURL *teamStatisticsURL = [NSURL URLWithString:STATISTICS_URL];
    NSURLRequest *teamStatisticsRequest = [NSURLRequest requestWithURL:teamStatisticsURL];
    
    AFJSONRequestOperation *teamStatisticsOperation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:teamStatisticsRequest
    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
       {
           //update team statistics.
           NSString *teamID = [self.userDefaults objectForKey:kBCTeamID];
           for (NSDictionary *teamData in (NSArray*)JSON)
           {
               if ([teamData[kBCTeamID] isEqualToString:teamID])
               {
                   if (!_teamStatistics)
                       self.teamStatistics = [NSMutableDictionary dictionary];
                   _teamStatistics = [teamData mutableCopy];
                   //NSLog(@"team statistics: %@",_teamStatistics);
               }
           }
           
       } failure:nil];
    [teamStatisticsOperation start];    
     
    */
}

-(NSDictionary*)mandatoryFields
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            @"1", kBCPlatformID,
            @"14ec4f3fb4020da07a96d3617c52ebfae83e1bcd", kBCID,
            @"1", kBCTeamID,
            nil];
}

-(void)syncKeyValuePairs:(NSDictionary*) keyValuePairs
{
    /* sync user data */    
    NSURL *syncURL = [NSURL URLWithString:SYNC_URL];
    NSMutableURLRequest *syncRequest = [NSMutableURLRequest requestWithURL:syncURL];
    [syncRequest setHTTPMethod:@"POST"];
    
    //Assemble POST string.
    
        int postKeyLenght = 0;
        NSMutableString *postString = [[NSMutableString alloc] initWithString:@""];
        
        //Add mandatory keys.
        for (NSString *key in [self.mandatoryFields keyEnumerator])
        {
            id value = [self.mandatoryFields objectForKey:key];
            NSString *stringValue = [self stringFromObject:value];
            
            postKeyLenght += [key length];
            NSString *toAppend = [NSString stringWithFormat:@"%@=%@&", key, stringValue];
            [postString appendString:toAppend];     
        }    
    
        //Add current keys.
        for (NSString *key in [keyValuePairs keyEnumerator])
        {
            NSString *backendKey = [self backendKeyForClientKey:key];
            if ([backendKey isEqual:COMING_SOON] == NO)
            {
                id value = [keyValuePairs objectForKey:key];
                NSString *stringValue = [self stringFromObject:value];
            
                postKeyLenght += [backendKey length];
                NSString *toAppend = [NSString stringWithFormat:@"%@=%@&", backendKey, stringValue];
                [postString appendString:toAppend];     
            }
        }
        
    //Create hash.
        
        NSLog(@"postKeyLenght <%i>", postKeyLenght);
    
        NSString *hashString = [[NSString stringWithFormat:@"%i", postKeyLenght] md5];
        NSString *hashToAppend = [NSString stringWithFormat:@"%@=%@", kBCHash, hashString];
        [postString appendString:hashToAppend];
    
    //Request.
    
        [syncRequest setValue:[NSString
                               stringWithFormat:@"%d", [postString length]]
           forHTTPHeaderField:@"Content-length"];
        [syncRequest setHTTPBody:[postString
                                  dataUsingEncoding:NSUTF8StringEncoding]];
        AFJSONRequestOperation *syncOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:syncRequest
                                                                                                success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON)
        {
            [self.delegate backendDidFinishSyncWithResponse:response json:JSON];
        } failure:nil];
        [syncOperation start];
}

-(NSString*)stringFromObject:(id) object
{
    if ([object isKindOfClass:[NSDate class]])
        return [self.dateFormatter stringFromDate:(NSDate*)object];

    if ([object isKindOfClass:[NSNumber class]])
        return [(NSNumber*)object stringValue];
    
    return (NSString*)object;
}

@end
