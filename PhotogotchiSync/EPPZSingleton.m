//
//  EPPZSingleton.m
//  EPPZKit
//
//  Created by Borbás Geri on 1/23/12.
//  Copyright (c) 2012 eppz! development, LLC. All rights reserved.
//


#import "EPPZSingleton.h"


NSMutableDictionary *_sharedEPPZSingletonInstances = nil;


@implementation EPPZSingleton
@synthesize delegates = _delegates;


#pragma mark - Delegate weak messaging

-(void)addDelegate:(id) object
{
    [_delegates addObject:object];
}

-(void)removeDelegate:(id) object
{   
    [_delegates removeObject:object];
}

-(void)messageDelegates:(SEL) selector
{
    [self messageDelegates:selector withObject:nil];    
}

-(void)messageDelegates:(SEL) selector withObject:(id) object;
{    
    [self messageDelegates:selector withObject:object withObject:nil];
}

-(void)messageDelegates:(SEL) selector withObject:(id) object withObject:(id) anotherObject;
{
    for (id eachDelegate in _delegates)
    {
        if ([eachDelegate respondsToSelector:selector])
            [eachDelegate performSelector:selector withObject:object withObject:anotherObject];    
    }
}

-(id)queryLastDelegate:(SEL) selector
{
    return [self queryLastDelegate:selector withObject:nil];
}

-(id)queryLastDelegate:(SEL)selector withObject:(id)object
{
    return [self queryLastDelegate:selector withObject:object withObject:nil];
}

-(id)queryLastDelegate:(SEL)selector withObject:(id) object withObject:(id) anotherObject
{
    id delegate = [_delegates lastObject];
    
    if (delegate != nil)
        if ([delegate respondsToSelector:selector])
            return [delegate performSelector:selector withObject:object withObject:anotherObject];
    
    return nil;
}


#pragma mark - Lifecycle

-(id)init
{
    NSLog(@"INIT.");
    
    if (self = [super init])
    {
        _delegates = [NSMutableArray new];
    }
    return self;
}

-(void)dealloc
{
    [self.class.sharedInstances removeObjectForKey:self.class.className];    
    
    [_delegates removeAllObjects];
    [_delegates release], _delegates = nil;
    
    [super dealloc];
}


#pragma mark - Singleton class setup

+(NSString*)className
{
    return NSStringFromClass(self);
}

+(NSMutableDictionary*)sharedInstances
{
    if (_sharedEPPZSingletonInstances == nil) _sharedEPPZSingletonInstances = [NSMutableDictionary new];
    return _sharedEPPZSingletonInstances;
}

+(id)sharedInstance
{    
    id soleInstanceForClass = [self.sharedInstances objectForKey:self.className];
    
    /*
    NSLog(@"EPPZSingleton +(id)sharedInstance");
    NSLog(@"self.class <%@>", [self class]);   
    NSLog(@"self.superclass <%@>", [self superclass]);      
    NSLog(@"soleInstanceForClass.class %@", soleInstanceForClass.class);
    NSLog(@"soleInstanceForClass.hash, %i", soleInstanceForClass.hash);  
    */
    
    if (soleInstanceForClass == nil )
    {
        soleInstanceForClass = [NSAllocateObject([self class], 0, NULL) init]; //via Carlo Chung.
        [self.sharedInstances setObject:soleInstanceForClass forKey:self.className];
    } 
    
	return soleInstanceForClass;
}

+(id)allocWithZone:(NSZone*) zone { return [[self sharedInstance] retain]; }
-(id)copyWithZone:(NSZone*) zone { return self; }
-(id)retain { return self; }
-(NSUInteger)retainCount { return NSUIntegerMax; }
-(oneway void)release { }
-(id)autorelease { return self; }


@end
