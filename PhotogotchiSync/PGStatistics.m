//
//  PGStatistics.m
//  PhotogotchiSync
//
//  Created by Borbás Geri on 9/1/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import "PGStatistics.h"

@implementation PGStatistics
@synthesize teams = _teams;

-(id)init
{
    if (self = [super init])
    {
        //Meant to be an array of team data.
        self.teams = [NSMutableDictionary new];
    }
    return self;
}

//Will make calculations, and stuff with neat accessors/types.


@end
