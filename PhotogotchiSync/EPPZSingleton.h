//
//  EPPZSingleton.h
//  EPPZKit
//
//  Created by Borbás Geri on 1/23/12.
//  Copyright 2012 eppz! development, LLC. All rights reserved.
//


#define EPPZ [EPPZSingleton sharedInstance]


@interface EPPZSingleton : NSObject

@property (nonatomic, retain, readonly) NSMutableArray *delegates;

+(id)sharedInstance;

-(void)addDelegate:(id) object;
-(void)removeDelegate:(id) object;

@end
