//
//  PGTeamData.h
//  PhotogotchiSync
//
//  Created by Borbás Geri on 9/1/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGTeamData : NSObject

@property (nonatomic) int ID;
@property (nonatomic) int userCount;
@property (nonatomic) int photoCount;
@property (nonatomic) int facebookShareCount;
@property (nonatomic) int twitterShareCount;
@property (nonatomic) int tumblrShareCount;

@end
