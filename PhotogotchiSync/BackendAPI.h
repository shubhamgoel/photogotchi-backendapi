//
//  BackendAPI.h
//  PhotogotchiSync
//
//  Created by Shubham Goel on 31/08/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (BackendAPIDelegate)
-(void)backendDidFinishSyncWithResponse:(NSHTTPURLResponse*) response json:(id) JSON;
-(void)backendDidFinishDownloadingTeamStatistics;
@end


#define BACKEND [BackendAPI sharedInstance]


#define STATISTICS_URL @"http://kissi.photogotchi.bear.carnation.hu/json/stat.json"
#define SYNC_URL @"http://kissi.photogotchi.bear.carnation.hu/photogochi/sync"
#define PLATFORM_ID @"1"


//Mandatory fields.
#define kBCPlatformID @"platform_id" //int 
#define kBCID @"udid" //string
#define kBCTeamID @"team_id" //0,1,2
#define kBCHash @"hash"

/* keys Team */
#define kBCUserCount @"userCount" //int
#define kBCUserPercentage @"userPercentage" //float
#define kBCPhotoPercentage @"photoPercentage" //float
#define kBCFacebookSharePercentage @"facebookSharePercentage" //float
#define kBCTwitterSharePercentage @"twitterSharePercentage" //float
#define kBCTumblrSharePercentage @"tumblrSharePercentage" //float

//Won't use for a while.
#define kBCDeviceToken @"device_token"


@interface BackendAPI : NSObject


@property (nonatomic, retain) NSMutableDictionary *userDefaults;
@property (nonatomic, retain) NSMutableDictionary *teamStatistics;

@property (nonatomic, assign) id delegate;



/* API calls */
// sync the data with the remote server and backs it up to NSUserDefaults

+(id)backendWithDelegate:(id) delegate;

-(void)syncKeyValuePairs:(NSDictionary*) keyValuePairs;
-(void)getStatistics;

@end
