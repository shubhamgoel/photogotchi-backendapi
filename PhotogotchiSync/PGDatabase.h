//
//  PGDatabase.h
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//


#import "PGSettings.h"
#import "PGUserData.h"
#import "PGStatistics.h"

#import "BackendAPI.h"


#define DATABASE [PGDatabase sharedDatabase]


@interface NSObject (PGDatabaseDelegate)
-(void)databaseDidLoad;
@end



@interface PGDatabase : EPPZSingleton

@property (nonatomic, strong) PGSettings *settings;
@property (nonatomic, strong) PGUserData *user;
@property (nonatomic, strong) PGStatistics *statistics;

+(PGDatabase*)sharedDatabase; //Only for typecasting.
-(void)takeOff;

@end
