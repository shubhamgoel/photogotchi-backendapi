//
//  PGSettings.h
//  PhotogotchiSync
//
//  Created by Borbás Geri on 9/1/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGSettings : NSObject

//To be KVO-compilant.
@property (nonatomic, strong) NSNumber *saveToLibraryNumber;
@property (nonatomic, strong) NSNumber *facebookIsOnNumber;
@property (nonatomic, strong) NSNumber *twitterIsOnNumber;
@property (nonatomic, strong) NSNumber *tumblrIsOnNumber;

@property (nonatomic) BOOL saveToLibrary;
@property (nonatomic) BOOL facebookIsOn;
@property (nonatomic) BOOL twitterIsOn;
@property (nonatomic) BOOL tumblrIsOn;

@end
