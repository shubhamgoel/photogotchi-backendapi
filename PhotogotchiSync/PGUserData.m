//
//  PGUserData.m
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import "PGUserData.h"

@implementation PGUserData

@synthesize UDID = _UDID;
@synthesize creature = _creature;
@synthesize totalCount = _totalCount;
@synthesize dailyCount = _dailyCount;

@synthesize firstPhotoLocationString = _firstPhotoLocationString;
@synthesize firstPhotoLocation = _firstPhotoLocation;
@synthesize firstPhotoDate = _firstPhotoDate;

@synthesize lastModificationDate = _lastModificationDate;
@synthesize lastSyncDate = _lastSyncDate;

-(id)init
{
    if (self = [super init])
    {
        self.creature = [PGCreatureData new];
        self.totalCount = [PGUserCounters new];
        self.dailyCount = [PGUserCounters new];        
    }
    return self;
}

-(void)setFirstPhotoLocation:(CGPoint) firstPhotoLocation
{ self.firstPhotoLocationString = NSStringFromCGPoint(firstPhotoLocation); }

-(CGPoint)firstPhotoLocation
{ return CGPointFromString(self.firstPhotoLocationString); }


@end
