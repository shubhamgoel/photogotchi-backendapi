//
//  PGDatabase.m
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import "PGDatabase.h"


static NSString *kNewValueKey = @"new";


@interface PGDatabase ()

@property (nonatomic, assign) NSUserDefaults *userDefaults;
@property (nonatomic, strong) BackendAPI *backend;
@property (nonatomic, strong) NSDictionary *keyPaths; 

-(void)populateDefaultLocalValues;
-(void)populateRuntimeStore;
-(void)addObservables;

@end


@implementation PGDatabase
@synthesize keyPaths = _keyPaths;

@synthesize settings = _settings;
@synthesize user = _user;
@synthesize statistics = _statistics;

@synthesize userDefaults = _userDefaults;
@synthesize backend = _backend;


#pragma mark - Typecasting

+(PGDatabase*)sharedDatabase
{ return (PGDatabase*)[PGDatabase sharedInstance]; }


#pragma mark - Setup

-(void)takeOff
{
    self.userDefaults = [NSUserDefaults standardUserDefaults];   
    
    [self populateDefaultLocalValues];
    [self populateRuntimeStore];
    
        self.backend = [BackendAPI backendWithDelegate:self];     
    
    //TODO: Sync user data to server if the userData.lastSyncDate - userData.lastModificationDate > a given limit.
    
    [self addObservables];  
    [self messageDelegates:@selector(databaseDidLoad)];
}

-(void)populateDefaultLocalValues
{
    NSDictionary *defaults = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"DefaultUserData" ofType:@"plist"]];
    [self.userDefaults registerDefaults:defaults];
    [self.userDefaults synchronize];
}

-(void)populateRuntimeStore
{
    self.user = [PGUserData new];
    self.statistics = [PGStatistics new];
    self.settings = [PGSettings new];
    
    for (NSString *eachKeyPath in [self.keyPaths keyEnumerator])
    {
        id value = [self storedValueForKeyPath:eachKeyPath];     
        
        @try
        {
            [self setValue:value forKeyPath:eachKeyPath];
        }
        
        @catch (NSException *exception)
        {
            //NSLog(@"%@", exception);
        }
        
        @finally
        {
            
        }
    }
}


#pragma mark - Content

    #define REMOTE [NSNumber numberWithBool:YES]
    #define LOCAL [NSNumber numberWithBool:NO]

-(NSDictionary*)keyPaths
{
    if (_keyPaths != nil) return _keyPaths;
    
    NSDictionary *keyPaths = [NSDictionary dictionaryWithObjectsAndKeys:
                              
    REMOTE, @"user.UDID",
    REMOTE, @"user.firstPhotoLocationString",
    REMOTE, @"user.firstPhotoDate",

    REMOTE, @"user.creature.ID",
    REMOTE, @"user.creature.teamID",                              
    REMOTE, @"user.creature.name",
    REMOTE, @"user.creature.points",
    REMOTE, @"user.creature.level",
    REMOTE, @"user.creature.birthdayDate",

    REMOTE, @"user.totalCount.photo",
    REMOTE, @"user.totalCount.daytimePhoto",
    REMOTE, @"user.totalCount.morningPhoto",
    REMOTE, @"user.totalCount.nightPhoto",
    REMOTE, @"user.totalCount.rearPhoto",
    REMOTE, @"user.totalCount.frontPhoto",
    REMOTE, @"user.totalCount.launch",
    REMOTE, @"user.totalCount.rename",
    REMOTE, @"user.totalCount.refer",
    REMOTE, @"user.totalCount.facebookShare",
    REMOTE, @"user.totalCount.twitterShare",                         
    REMOTE, @"user.totalCount.tumblrShare", 

    LOCAL, @"user.dailyCount.photo",
    LOCAL, @"user.dailyCount.daytimePhoto",
    LOCAL, @"user.dailyCount.morningPhoto",
    LOCAL, @"user.dailyCount.nightPhoto",
    LOCAL, @"user.dailyCount.rearPhoto",
    LOCAL, @"user.dailyCount.frontPhoto",
    LOCAL, @"user.dailyCount.launch",
    LOCAL, @"user.dailyCount.rename",
    LOCAL, @"user.dailyCount.refer",
    LOCAL, @"user.dailyCount.facebookShare",
    LOCAL, @"user.dailyCount.twitterShare",                         
    LOCAL, @"user.dailyCount.tumblrShare",    

    LOCAL, @"settings.saveToLibraryNumber",
    LOCAL, @"settings.facebookIsOnNumber",
    LOCAL, @"settings.twitterIsOnNumber",
    LOCAL, @"settings.tumblrIsOnNumber",                         

    nil];
    
    _keyPaths = keyPaths;
    return _keyPaths;
}

-(BOOL)shouldSyncKeyPath:(NSString*) keyPath
{
    return (BOOL)[[self.keyPaths objectForKey:keyPath] boolValue];
}


#pragma mark - Observing runtime store (store remote/local values)

-(void)addObservables
{
    NSObject *observer = (NSObject*)self;
    for (NSString *eachKeyPath in [self.keyPaths keyEnumerator])
        [self addObserver:observer
               forKeyPath:eachKeyPath
                  options:NSKeyValueObservingOptionNew
                  context:nil];
}

    #define NOW [NSDate date]

-(void)observeValueForKeyPath:(NSString*) keyPath ofObject:(id)object change:(NSDictionary*) change context:(void*) context
{
    id value = [change objectForKey:kNewValueKey];
    
    //Store the value local.
    [self storeValue:value forKeyPath:keyPath];
    
    //Refresh local timestamp.
    [self storeValue:NOW forKeyPath:@"user.lastModificationDate"];
    
    //Sync this field to server, if we have connection.
    [self.backend syncKeyValuePairs:[NSDictionary dictionaryWithObjectsAndKeys:value, keyPath, nil]];
}

-(void)backendDidFinishSyncWithResponse:(NSHTTPURLResponse*) response json:(id) JSON
{
    NSLog(@"%@", response);
    
    NSLog(@"%@", JSON);
    
    //Refresh remote timestamp.
    //[self storeValue:NOW forKeyPath:@"user.lastSyncDate"];    
}


#pragma mark - Local store (NSUserDefaults for now)

-(void)storeValue:(id) object forKeyPath:(NSString*) keyPath
{
    [self.userDefaults setObject:(id) object forKey:keyPath];
    [self.userDefaults synchronize];
}

-(id)storedValueForKeyPath:(NSString*) keyPath
{
    return [self.userDefaults objectForKey:keyPath];
}

@end
