//
//  EPPZSingletonSubclass.h
//  EPPZKit
//
//  Created by Borbás Geri on 5/26/12.
//  Copyright (c) 2011 eppz! development, LLC. All rights reserved.
//


#import "EPPZSingleton.h"


extern NSMutableDictionary *_sharedEPPZSingletonInstances;


@interface EPPZSingleton (Protected)

+(NSString*)className;

-(void)messageDelegates:(SEL) selector;
-(void)messageDelegates:(SEL) selector withObject:(id) object;
-(void)messageDelegates:(SEL) selector withObject:(id) object withObject:(id) anotherObject;

-(id)queryLastDelegate:(SEL) selector;
-(id)queryLastDelegate:(SEL) selector withObject:(id) object;
-(id)queryLastDelegate:(SEL) selector withObject:(id) object withObject:(id) anotherObject;


@end
