//
//  PGUserData.h
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PGCreatureData.h"
#import "PGUserCounters.h"


@interface PGUserData : NSObject

@property (nonatomic, strong) NSString *UDID;
@property (nonatomic, strong) PGCreatureData *creature;
@property (nonatomic, strong) PGUserCounters *totalCount;
@property (nonatomic, strong) PGUserCounters *dailyCount;

@property (nonatomic, strong) NSString* firstPhotoLocationString; //KVO.
@property (nonatomic) CGPoint firstPhotoLocation; //Clients.
@property (nonatomic, strong) NSDate *firstPhotoDate;

@property (nonatomic, strong) NSDate *lastModificationDate;
@property (nonatomic, strong) NSDate *lastSyncDate;

@end
