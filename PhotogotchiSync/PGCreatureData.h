//
//  PGCreatureData.h
//  camera
//
//  Created by Borbás Geri on 8/31/12.
//  Copyright (c) 2012 Pangalaktik Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PGCreatureData : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSNumber *teamID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic) int points;
@property (nonatomic) int level;
@property (nonatomic, strong) NSDate *birthdayDate;

@end
