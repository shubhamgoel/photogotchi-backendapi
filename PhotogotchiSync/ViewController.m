//
//  ViewController.m
//  PhotogotchiSync
//
//  Created by Shubham Goel on 31/08/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)userActionsTouchedUp:(id) sender
{
    DATABASE.user.totalCount.photo++;
    DATABASE.user.creature.name = @"Shubham";
    DATABASE.user.dailyCount.twitterShare++;
    //And so on.
}
@end
