//
//  ViewController.h
//  PhotogotchiSync
//
//  Created by Shubham Goel on 31/08/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

-(IBAction)userActionsTouchedUp:(id) sender;

@end
