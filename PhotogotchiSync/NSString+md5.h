//
//  NSString+md5.h
//  PhotogotchiSync
//
//  Created by Shubham Goel on 31/08/12.
//  Copyright (c) 2012 Shubham Goel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (md5)
- (NSString *) md5;
@end
